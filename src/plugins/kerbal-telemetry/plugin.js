/*******************************************************************
 * MIT License
 *
 * Copyright (c) 2021-2022 Lukas Voreck
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************/

import objectProvider from './object-provider';
import {rootProvider, firstSubdirProvider, secondSubdirProvider} from './composit-providers';
import telemetryProvider from './telemetry-provider';
const telemetryType = require("./type.json");

export default function plugin(ip = '127.0.0.1', port = '8085') {
    return function install(openmct) {
        openmct.objects.addRoot({
            namespace: 'vck.zll',
            key: 'rootdir'
        });

        openmct.objects.addProvider('vck.zll', objectProvider);

        openmct.composition.addProvider(rootProvider);
        openmct.composition.addProvider(firstSubdirProvider);
        openmct.composition.addProvider(secondSubdirProvider);

        telemetryProvider.init(ip, port);
        openmct.telemetry.addProvider(telemetryProvider.provider);

        openmct.types.addType('kerbal.telemetry', telemetryType);
    };
}
