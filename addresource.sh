#!/bin/bash

cd "${0%/*}" || exit

VALUES_JSON="      \"values\": [
        {
          \"key\": \"value\",
          \"name\": \"Value\",
          \"format\": \"float\",
          \"hints\": {
            \"range\": 1
          }
        },
        {
          \"key\": \"utc\",
          \"source\": \"timestamp\",
          \"name\": \"Timestamp\",
          \"format\": \"utc\",
          \"hints\": {
            \"domain\": 1
          }
        }
      ]
"

NAME=$1
KEY=${1/ /}

RESOURCE_JSON="    {
      \"name\": \"$NAME\",
      \"key\": \"r.resource.$KEY\",
$VALUES_JSON    },
"
RESOURCE_CURRENT_JSON="    {
      \"name\": \"$NAME in current Stage\",
      \"key\": \"r.resourceCurrent.$KEY\",
$VALUES_JSON    },
"
RESOURCE_CURRENT_MAX_JSON="    {
      \"name\": \"Max $NAME in current Stage\",
      \"key\": \"r.resourceCurrentMax.$KEY\",
$VALUES_JSON    },
"
RESOURCE_MAX_JSON="    {
      \"name\": \"Max $NAME\",
      \"key\": \"r.resourceMax.$KEY\",
$VALUES_JSON    }
"

R_PATH="./src/plugins/kerbal-telemetry/dictionaries/r.json"
R_JSON="$(head -n -3 $R_PATH)    },
${RESOURCE_JSON}${RESOURCE_CURRENT_JSON}${RESOURCE_CURRENT_MAX_JSON}${RESOURCE_MAX_JSON}  ]
}"

echo "$R_JSON" > "$R_PATH"

