# Kerbal Telemetry
------------------------------------------------------------------------------------------------------------------------

There's one thing Kerbal Space Program is missing. It allows you to build and fly your own rockets, to explore the solar
system, even our own with [RSS](https://forum.kerbalspaceprogram.com/index.php?/topic/177216-18-real-solar-system-v1815-27-jun-2021/)
And you can build even more realistic rockets with [Realism Overhaul](https://forum.kerbalspaceprogram.com/index.php?/topic/155700-1101-realism-overhaul-27-jul-2021/).

But there is one aspect of spaceflight missing that Kerbal Telemetry is trying to provide: Mission Control.

This software, based on NASAS open source mission control software [openmct](https://github.com/nasa/openmct), allows you
to set up mission control on your own PC! 

------------------------------------------------------------------------------------------------------------------------

## Installing, Running and Updating

------------------------------------------------------------------------------------------------------------------------

### Installing

First thing you'll need is the Telemachus mod for ksp. you can either get it from CKAN or get [ericjdevelopers fork](https://github.com/ericjdeveloper/Telemachus-1).
In the future it will probably only work with his fork because Telemachus isn't in developement anymore and were 
planning to clean some things up and add new features but for now the standard Telemachus Reborn mod from CKAN works fine

Once you got the mod, you need to install [Node.js](https://nodejs.org/) on your pc. This is the framework openmct uses.

Download and unzip the source of one of the [releases](https://gitlab.com/Overloader/kerbal-telemetry/-/releases) (If youre 
unsure which one you'll probably want to download the "Source code (zip)" of the newest release) or, if youre more tech 
savy, clone this repository.

Once you got the source code you need to open a command line in the kerbal-telemetry directory, type in `npm install` 
and hit enter. This will install all dependencies openmct needs. Sometimes this stop at `webpack compiled` followed by 
a number of errors. If you hit that point and it doesnt finish itself, it's safe to stop it by pressing `Ctrl+C`

------------------------------------------------------------------------------------------------------------------------

### Running

Once again, open a command line in the kerbal-telemetry directory and type in `npm start`, then hit enter. This starts
openmct. Once its running, leave the command line open and open your browser, then go to http://localhost:8080/ and 
you're there! When you're done playing KSP, close the site, head back into the command prompt where openmct is running
and press `Ctrl + C`.

There are two things to note once you're on the site: When you are trying to look at telemetry and can't see anything, 
first try to see if on the lower left corner it says `Fixed Timespan` or `Local Clock`. You'll only see telemtry when 
it's set on local clock.

Theres also an annoying warning about `Missing request provider, see console for details`. It's obnoxious but for now 
just ignore it, thats going to get fixed at a later date.

------------------------------------------------------------------------------------------------------------------------

### Updating

If you downloaded the source code from the [releases](https://gitlab.com/Overloader/kerbal-telemetry/-/releases), just
download the new version from there, delete the old kerbal-telemetry directory and replace it with the new one. All your
layouts, settings and stuff like that should be saved. Then run `npm install` again.

If you used git to clone this repo, just run a `git pull` followed up by a `npm install` in case there are new/updated 
dependencies and you're good to go. 

------------------------------------------------------------------------------------------------------------------------

## Using Kerbal Telemetry

Nasa has a nice [Live Demo](https://openmct-demo.herokuapp.com/) that you can use to learn the basics of what openmct 
has to offer

------------------------------------------------------------------------------------------------------------------------

## Add your own Resources

KSP Openmct is able to track all the stock Resources like Liquid Fuel, Oxidizer etc.

But what if you play with mods and have other resources like Kerosene? Well it's simple. Just go into the ksp openmct 
directory, open a command prompt and type `addResource.ps1 "Your Resource"` on windows or `./addResource.sh "Your Resource"` 
on linux (I don't own an apple product so I can't tell you how to do it there). Note that the quotation marks are important

When you update Kerbal Telemetry you either have to add your resources again or make a backup of the following file: 
`ksp-openmct/src/plugins/kerbal-telemetry/dictionaries/r.json`

************************************************************************************************************************

## Contributing.
If you want to help either this project or [ericjdevelopers fork of telemachus](https://github.com/ericjdeveloper/Telemachus-1) 
join us on [Discord](https://discord.gg/6uAmwWsCv7)!

------------------------------------------------------------------------------------------------------------------------

## Credits
Currently all of this is written by myself but in the future there might be contributions by others. 

Also I will be working together with [ericjdeveloper](https://github.com/ericjdeveloper?tab=overview&from=2021-12-01&to=2021-12-31)
who is creating a [fork of Telemachus](https://github.com/ericjdeveloper/Telemachus-1) to add new features.


This project is a fork of NASAS [openmct](https://github.com/nasa/openmct) and the biggest part of the code belongs to
NASA. Thank you, NASA for making this amazing piece of software open source!

------------------------------------------------------------------------------------------------------------------------

## MIT License

Copyright (c) 2021-2022 Lukas Voreck

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
